import React from "react";
import {LogIn, Register} from "./pages/auth";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import Dashboard from "./layouts/Dashboard";
import SecretRoute from "./components/SecretRoute";
import E404 from "./pages/errors/E404";

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/'><Redirect to="/articles"/></Route>
                <Route exact path='/login' component={LogIn}/>
                <Route exact path='/register' component={Register}/>
                <SecretRoute path='/articles' component={Dashboard}/>
                <SecretRoute exact path='/articles/page/:currentPage' component={Dashboard}/>

                <Route from='*' component={E404}/>
            </Switch>
        </BrowserRouter>
    )
}

export default App;
