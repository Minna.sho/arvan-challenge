export const setAllArticles = payload => {
    return {
        type: 'SET_ALL_ARTICLES',
        payload
    };
};