export const login = token => {
    return {
        type: 'LOGIN',
        payload: {
            token,
        },
    };
};

export const logout = () => {
    return {
        type: 'LOGOUT',
    };
};

export const setUserInfo = payload => {
    return {
        type: 'SET_USER_INFO',
        payload,
    };
};