import axios from 'axios';
import store from "./store";

const base_url = 'https://conduit.productionready.io/api/';

export const client = axios.create({baseURL: base_url, json: true, timeout: 120000});

const call = async (method, url, data = {}) => {
    const token = store.getState().auth.token;
    const headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    };

    if (!!token) {
        headers.Authorization = `Token ${token}`;
    }

    const request = {headers, method, url};

    if (Object.keys(data).length > 0) {
        request.data = data;
    }

    try {
        const response = await client(request);
        return Promise.resolve(response.data);
    } catch (error) {
        if (error.hasOwnProperty('response') && !!(error.response)) {
            return Promise.reject(error.response);
        }
        if (error.hasOwnProperty('response') && (!error.response)) {
            const errors = {
                data: {
                    networkError: 'an Error accured, please check your network connection and try again later'
                }
            };
            return Promise.reject(errors);
        }
        return Promise.reject(error.response);
    }
};

const auth = {
    login: data => call("post", "users/login", data),
    register: data => call("post", "users", data),
};

const dashboard = {
    listOfArticles: (limit, offset) => call("get", `articles?limit=${limit}&offset=${offset}`),
    getArticle: (slug) => call("get", `articles/${slug}`),
    createArticle: (data) => call("post", "articles", data),
    updateArticle: (slug, data) => call("put", `articles/${slug}`, data),
    deleteArticle: (slug) => call("delete", `articles/${slug}`),
    getTags: () => call("get", "tags")
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    ...auth,
    ...dashboard,
    post: (url, data = {}) => call("post", url, data),
    get: (url, data = {}) => call("get", url, data),
    put: (url, data = {}) => call("put", url, data),
    patch: (url, data = {}) => call("patch", url, data),
    delete: (url, data = {}) => call("delete", url, data)
};