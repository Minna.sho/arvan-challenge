import React, {useCallback, useEffect, useState} from "react";
import {Spinner} from "react-bootstrap";
import moment from 'moment'
import api from "../api";
import {PaginationUtils} from '../modules/pagination-module';
import Pagination from "react-js-pagination";
import MoreOptions from "./MoreOptions";
import {useSnackbar} from "notistack";
import globalErrors from "../modules/api-errors";
import {useHistory, useParams} from "react-router";


const ArticleTable = () => {
    const {enqueueSnackbar} = useSnackbar();
    const [pageData, setPageData] = useState(null);
    const [articles, setAllArticles] = useState(null);

    let {currentPage} = useParams();
    currentPage = parseInt(currentPage);
    currentPage = currentPage > 1 ? currentPage : 1;

    const [page, setPage] = useState(currentPage);

    const history = useHistory();

    const offset = PaginationUtils.createOffset(page, 10);

    const applyPageData = (limit, totalRecords) => {
        const a = {
            itemsCountPerPage: limit,
            totalItemsCount: totalRecords,
        };
        setPageData(prev => ({...prev, ...a}));
    };

    const fetcher = useCallback(() => {

        const offset = PaginationUtils.createOffset(page, 10);
        setAllArticles(null);
        api.listOfArticles(10, offset)
            .then(res => {
                console.log('here', page, res);
                setAllArticles(res.articles);
                if (pageData === null)
                    applyPageData(10, res?.['articlesCount']);
            })
            .catch(err => {
                let errors = err.hasOwnProperty('data') ? err.data.errors : null;
                if (!!errors) {
                    Object.keys(errors).map((err) => {
                        return enqueueSnackbar(err + ' ' + errors[err], {
                            variant: "error"
                        });
                    });
                }
            });

        //eslint-disable-next-line
    }, [page]);

    useEffect(fetcher, [page, fetcher]);

    const handleDelete = (slug, handleClose) => {
        handleClose();
        api
            .deleteArticle(slug)
            .then(() => {
                enqueueSnackbar('Article deleted successfuly', {
                    variant: "success"
                });
                fetcher();
            })
            .catch(err => {
                handleClose();
                let errors = err.hasOwnProperty('data') ? err.data.errors : null;
                let status = err.hasOwnProperty('status') ? err.status : null;
                if (!!status) {
                    let arrayOfStatus = Object.keys(globalErrors);
                    if (arrayOfStatus.indexOf(status.toString()) !== -1) {
                        return enqueueSnackbar(globalErrors[status], {
                            variant: "error"
                        })
                    }
                }
                if (!!errors) {
                    Object.keys(errors).map((err) => {
                        return enqueueSnackbar(err + ' ' + errors[err], {
                            variant: "error"
                        });
                    });
                }
            })
    };

    const handlePagination = (pageToBe) => {
        pageToBe = pageToBe > 1 ? pageToBe : 1;

        history.push(pageToBe > 1 ? `/articles/page/${pageToBe}` : '/articles');
        setPage(pageToBe);
    };
    useEffect(() => {
        setPage(currentPage);
        //eslint-disable-next-line
    }, [])

    return (
        <>
            <h1>All Posts</h1>
            <div className='article-table-wrapper table-responsive'>
                {articles === null
                    ?
                    <div className='text-center overflow-hidden my-5'>
                        <h3>Loading...</h3>
                        <Spinner animation="border" variant="info" size='lg'/>
                    </div>
                    :
                    <table className='table article-table'>
                        <thead className='article-table__header'>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Tags</th>
                            <th>Excerpt</th>
                            <th colSpan='2'>Created</th>
                        </tr>
                        </thead>
                        <tbody className='article-table__body'>
                        {articles.map((article, i) => {
                            return (
                                <tr key={i}>
                                    <td>{i + offset + 1}</td>
                                    <td>{article.title}</td>
                                    <td>{article.author.username}</td>
                                    <td>{article.tagList.toString()}</td>
                                    <td>{article.body.slice(0, 21)}</td>
                                    <td>
                                        {moment(article.createdAt.substr(0, article.createdAt.indexOf(':'))).format('ll')}
                                    </td>
                                    <td>
                                        <MoreOptions slug={article.slug} onDelete={handleDelete}/>
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                        <tfoot className='article-table__footer'>
                        <tr>
                            <td colSpan='7' align='center'>
                                <Pagination
                                    itemsCountPerPage={pageData?.itemsCountPerPage}
                                    totalItemsCount={pageData?.totalItemsCount}
                                    activePage={page}
                                    itemClass='page-item'
                                    linkClass='page-link'
                                    pageRangeDisplayed={10}
                                    onChange={handlePagination}
                                />
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                }
            </div>
        </>
    );
};


export default ArticleTable;
