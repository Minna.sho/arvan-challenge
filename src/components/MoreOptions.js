import React, {useState} from "react";
import {Button, Dropdown, DropdownButton, Modal} from "react-bootstrap";

const MoreOptions = ({slug, onDelete}) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return (
        <>
            <DropdownButton title='...' menuAlign='right'>
                <Dropdown.Item href={`/articles/${slug}`}>Edit</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item onClick={handleShow}>Delete</Dropdown.Item>
            </DropdownButton>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete Article</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure to delete Article?</Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose}>
                        No
                    </Button>
                    <Button variant="danger" onClick={() => onDelete(slug, handleClose)}>
                        Yes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default MoreOptions;
