import React from "react";
import {Alert} from "react-bootstrap";

const Notification = ({message, variant}) => {
    return(
        <Alert variant={variant} className='notification'>
            {message}
        </Alert>
    )
};

export default Notification;
