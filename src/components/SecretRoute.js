import React from "react";
import {Route, Redirect} from "react-router-dom";
import store from "../store";

const SecretRoute = ({component: Component, path, rest}) => {
    const loggedIn = store.getState().auth.loggedIn;

    return (
        <Route
            path={path}
            {...rest}
            render={props =>
                loggedIn ? <Component {...props} /> : <Redirect to="/login"/>
            }
        />
    );
};

export default SecretRoute;
