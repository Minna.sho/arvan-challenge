import React, { useRef } from "react";
import { SnackbarProvider } from "notistack";

const Snack = ({ children }) => {
  const notistack = useRef();

  const handleDismiss = key => {
    notistack.current.closeSnackbar(key);
  };

  return (
    <SnackbarProvider
      ref={notistack}
      action={key => {
        return (
          <button className="ic-btn" onClick={() => handleDismiss(key)}>
              <i className="fa fa-close"></i>
          </button>
        );
      }}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
    >
      {children}
    </SnackbarProvider>
  );
};

export default Snack;
