import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import Snack from "./components/Snack";
import store from "./store"
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/styles/index.min.css';

ReactDOM.render(
    <Provider store={store}>
        <Snack>
            <App/>
        </Snack>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
