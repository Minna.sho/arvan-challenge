import React from "react";
import {Container} from "react-bootstrap";

const Auth = ({page, history, children}) => {

    const titles = {loginPage: 'LOGIN', registerPage: 'Register'};

    return (
        <Container>
            <div className='auth'>
                <div className='auth__header'>
                    <h2>{titles[page]}</h2>
                </div>
                <div className='auth__body'>
                    {children}
                </div>
                <div className='auth__footer'>
                    <span
                        className='auth__footer--Q'>{page === 'loginPage' ? 'Don’t have account' : 'Already Registered'}?</span>
                    <a href={page === 'loginPage' ? '/register' : '/login'}
                       className='auth__footer--link'>{page === 'loginPage' ? 'Register Now' : 'Login'}</a>
                </div>
            </div>
        </Container>
    );

};

export default Auth;
