import React, {useState} from "react";
import {connect} from "react-redux";
import {Navbar, Button, Container} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars, faPowerOff, faUser} from '@fortawesome/free-solid-svg-icons'
import ArticleTable from "../components/ArticleTable";
import {logout} from "../actions/auth";
import {Switch as RNSwitch, Route} from "react-router-dom";
import NewArticle from "../pages/dashboard/NewArticle";

const Dashboard = ({userInfo, match, logout, history}) => {
    const [userOptionsClicked, setUserOptionsClicked] = useState(false);
    const [barsClicked, setBarsClicked] = useState(false);

    const toggle = (controller) => {
        if (controller === 'userOptions')
            setUserOptionsClicked(!userOptionsClicked);
        else if (controller === 'sidebar')
            setBarsClicked(!barsClicked);
        else return;
    }

    const handleLogOut = () => {
        logout();
        history.push('/login');
    }

    return (
        <>
            <Navbar bg='dark' variant='dark' className='navbar'>
                <div className='navbar__intro'>
                    <Navbar.Brand className='navbar__intro--brand'>
                        <span className='icon-btn mr-2 d-sm-inline-block d-lg-none'
                              onClick={() => toggle('sidebar')}>
                            <FontAwesomeIcon className='text-light' icon={faBars}/>
                        </span>
                        Arvan Challenge
                    </Navbar.Brand>
                    <span className='navbar__intro--text'>
                        Welcom {userInfo.username}
                        <span className='icon-btn  ml-2 d-sm-inline-block d-lg-none'
                              onClick={() => toggle('userOptions')}>
                            <FontAwesomeIcon className='text-light' icon={faUser}/>
                        </span>
                    </span>
                </div>
                <Button className='navbar__logout-btn' variant='outline-primary'
                        onClick={handleLogOut}>Logout</Button>
            </Navbar>
            <div className={'user-menu' + (!userOptionsClicked ? ' d-none' : '')}>
                <div className='navbar__logout-btn--mobile'><FontAwesomeIcon onClick={handleLogOut} size={"2x"} icon={faPowerOff}/></div>
            </div>

            <div className={'sidebar' + (barsClicked ? ' d-block' : '')}>
                <h3 className='sidebar__title'>Post</h3>
                <div
                    className={'sidebar__link' + (history.location.pathname === '/articles' ? ' sidebar__link--active' : '')}>
                    <a href="/articles">All Articles</a></div>
                <div
                    className={'sidebar__link' + (history.location.pathname === '/articles/create' ? ' sidebar__link--active' : '')}>
                    <a href="/articles/create">New Article</a></div>
            </div>

            <Container>
                <div className="content">
                    <RNSwitch>
                        <Route exact path={`${match.path}/create`} component={NewArticle}/>
                        <Route exact path={`${match.path}/:slug`} component={NewArticle}/>
                        <Route path={`${match.path}`} component={ArticleTable}/>
                    </RNSwitch>
                </div>
            </Container>
        </>
    )
};


const mapStateToProps = state => ({
    userInfo: state.auth.userInfo,
});

const mapDispatchToProps = {
    logout
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
