
const globalErrors = {
    401:'Unauthorized requests',
    403:'Forbidden requests, you don\'t have permissions to perform the action',
    404:'Not found requests, a resource can\'t be found to fulfill the request',
};

export default globalErrors;
