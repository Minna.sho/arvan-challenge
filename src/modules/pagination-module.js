export const PaginationUtils = {

    /**
     *
     * @param totalRecords
     * @param limit Page Size
     * @returns {number}
     */
    numberOfPages(totalRecords, limit) {
        return Math.ceil(
            totalRecords / limit
        );
    },
    createOffset(page, limit) {
        return (page - 1) * limit;

    }
};