import React, {useState} from "react";
import Auth from "../../layouts/Auth";
import {Form, Button, Spinner} from "react-bootstrap";
import * as Yup from "yup";
import api from "../../api";
import {Formik} from "formik";
import handleFormikBlur from "../../modules/handleBlur";
import Notification from "../../components/Notification";
import {connect} from "react-redux";
import {login, setUserInfo} from "../../actions/auth";

const LogIn = ({history, login, setUserInfo}) => {
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);
    const [msg, setMsg] = useState('');

    const reqBody = {
        user: {}
    }
    const validationSchema = Yup.object({
        email: Yup.string()
            .email("Invalid email address")
            .required("Required field"),
        password: Yup.string()
            .required("Required field"),
    });

    const doSubmit = (values, {resetForm, setSubmitting}) => {
        reqBody.user = values;
        console.log('click', reqBody);
        setSubmitting(true);
        api
            .login({...reqBody})
            .then(res => { // eslint-disable-next-line import/no-anonymous-default-export
                login(res.user.token);
                setUserInfo(res.user);
                console.log(typeof (res.user.token), res.user.token);

                setSuccess(true)
                setSubmitting(false);
                resetForm({});

                history.push('/articles');
            })
            .catch(e => {
                resetForm({});
                setError(true);
                setSubmitting(false);
                let errors = e.hasOwnProperty('data') ? e.data.errors : null;
                if (!!errors) {
                    Object.keys(errors).map((err, i) => {
                        return setMsg(err + ' ' + errors[err]);
                    });
                }
            })
            .then(function () {
                resetForm({});
                setSubmitting(false);
            });
    };

    return (
        <Auth page='loginPage'>
            {error
                ? <Notification message={msg} variant='danger'/>
                : success ? <Notification message="You're Successfully Registered!" variant='success'/>
                    : false
            }
            <Formik initialValues={{email: "", password: ""}}
                    validationSchema={validationSchema}
                    onSubmit={doSubmit}>
                {({handleChange, handleSubmit, isSubmitting, handleBlur, touched, values, errors}) => (
                    <Form className='authform' onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail" className='authform__form-wrapper'>
                            <Form.Label className={'authform__form-wrapper--label' +
                            ((touched.email && errors.email) ? 'error' : '')}>
                                Email
                            </Form.Label>
                            <Form.Control type="email" name='email'
                                          value={values.email}
                                          onChange={handleChange}
                                          onBlur={(event) => handleFormikBlur(event, handleBlur)}
                                          className={touched.email && errors.email ? "authform__form-wrapper--error" : null}/>
                            {(touched.email && errors.email) ?
                                <small className='authform__form-wrapper--errormessage'>{errors.email}</small> : null}
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword" className='authform__form-wrapper'>
                            <Form.Label className={'authform__form-wrapper--label' +
                            ((touched.password && errors.password) ? 'error' : '')}>
                                Password
                            </Form.Label>
                            <Form.Control type="password" name='password'
                                          value={values.password}
                                          onChange={handleChange}
                                          onBlur={(event) => handleFormikBlur(event, handleBlur)}
                                          className={touched.password && errors.password ? "authform__form-wrapper--error" : null}/>
                            {(touched.password && errors.password) ?
                                <small
                                    className='authform__form-wrapper--errormessage'>{errors.password}</small> : null}
                        </Form.Group>

                        <Button variant="primary" type="submit" className='btn-block authform__button'
                                disabled={(isSubmitting || !!errors.password || !!errors.email || !!errors.username)}>
                            {isSubmitting ? <Spinner animation="border" variant="secondary" size='sm'/> : 'Login'}
                        </Button>
                    </Form>
                )}
            </Formik>
        </Auth>
    );
}

const mapDispatchToProps = {
    login,
    setUserInfo
};

export default connect(null, mapDispatchToProps)(LogIn);