import React, {useEffect, useState} from "react";
import {Button, Col, Form, Row, Spinner} from "react-bootstrap";
import handleFormikBlur from "../../modules/handleBlur";
import {Formik} from "formik";
import * as Yup from "yup";
import api from "../../api";
import {useSnackbar} from "notistack";
import globalErrors from "../../modules/api-errors";
import {isEmpty} from "../../modules/utils";
import {useHistory, useParams} from "react-router";
// import queryString from "query-string";

const NewArticle = () => {
    const {enqueueSnackbar} = useSnackbar();
    const [tagList, setTagList] = useState([]);
    const [newTag, setNewTag] = useState({});
    const [inputs, setInputs] = useState({
        title: '',
        description: '',
        body: '',
        tagList: [],
    });
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    // const location = useLocation();
    const {slug} = useParams();

    useEffect(() => {
        !!slug ?
            api.getArticle(slug)
                .then(res => {
                    setLoading(false);
                    setInputs({
                        title: res.article.title,
                        description: res.article.description,
                        body: res.article.body,
                        tagList: res.article.tagList
                    });
                    let array = [];
                    let tags = (res.article.tagList).sort();
                    tags.map((t) => {
                        if (!(!t || 0 === t.length))
                            return array.push({title: t, isChecked: true})
                        return null;
                    })
                    setTagList(array);
                })
                .catch(() => {
                    setLoading(false);
                })
            : api.getTags()
                .then(res => {
                    setLoading(false)
                    let array = [];
                    let tags = (res.tags).sort();
                    tags.map((t) => {
                        if (!(!t || 0 === t.length))
                            return array.push({title: t, isChecked: true})
                        return null;
                    })
                    setTagList(array);
                })
                .catch(() => {
                    setLoading(false)
                });
    }, [slug]);

    const reqBody = {
        article: {}
    }
    const validationSchema = Yup.object({
        title: Yup.string()
            .required('Required field'),
        description: Yup.string()
            .required("Required field"),
        body: Yup.string()
            .required("Required field"),
    });

    const doSubmit = (values, {resetForm, setSubmitting}) => {
        let approvedTags = [];
        tagList.map((tag) => {
            if (tag.isChecked) {
                return approvedTags.push(tag.title);
            }
            return null;
        });
        values.tagList = approvedTags;
        reqBody.article = values;
        setSubmitting(true);
        !!slug
            ? api
                .updateArticle(slug, {...reqBody})
                .then(() => {
                    setSubmitting(false);
                    resetForm({});
                    enqueueSnackbar('Well done! Article updated successfuly', {
                        variant: "success"
                    });
                    history.push('/articles');
                })
                .catch(err => {
                    resetForm({});
                    setSubmitting(false);

                    let errors = err.hasOwnProperty('data') ? err.data.errors : null;
                    let status = err.hasOwnProperty('status') ? err.status : null;
                    if (!!status) {
                        let arrayOfStatus = Object.keys(globalErrors);
                        if (arrayOfStatus.indexOf(status.toString()) !== -1) {
                            return enqueueSnackbar(globalErrors[status], {
                                variant: "error"
                            })
                        }
                    }
                    if (!!errors) {
                        Object.keys(errors).map((err) => {
                            return enqueueSnackbar(err + ' ' + errors[err], {
                                variant: "error"
                            });
                        });
                    }
                })
                .then(function () {
                    resetForm({});
                    setSubmitting(false);
                })

            : api
                .createArticle({...reqBody})
                .then(() => {
                    setSubmitting(false);
                    resetForm({});
                    enqueueSnackbar('Well done! Article created successfuly', {
                        variant: "success"
                    });
                    history.push('/articles');
                })
                .catch(err => {
                    resetForm({});
                    setSubmitting(false);

                    let errors = err.hasOwnProperty('data') ? err.data.errors : null;
                    let status = err.hasOwnProperty('status') ? err.status : null;
                    if (!!status) {
                        let arrayOfStatus = Object.keys(globalErrors);
                        if (arrayOfStatus.indexOf(status.toString()) !== -1) {
                            return enqueueSnackbar(globalErrors[status], {
                                variant: "error"
                            })
                        }
                    }
                    if (!!errors) {
                        Object.keys(errors).map((err) => {
                            return enqueueSnackbar(err + ' ' + errors[err], {
                                variant: "error"
                            });
                        });
                    }
                })
                .then(function () {
                    resetForm({});
                    setSubmitting(false);
                });
    };

    const doChange = (event) => {
        let value = event.target.value;
        let objTag = {title: value, isChecked: true}
        setNewTag(objTag);
    };
    const addToTagList = () => {
        setTagList([...tagList, newTag]);
        setNewTag('');
        let input = document.getElementsByClassName('tag__input')[0];
        input.value = '';
    }
    const modifyTag = (tag) => {
        const tags = [...tagList];
        const index = tags.indexOf(tag);
        tags[index] = {...tags[index]};
        tagList[index].isChecked = !(tagList[index].isChecked);
        setTagList(tagList);
    }

    return (
        <>
            <h1>New Article</h1>
            <Row>
                <Col className='col-md-6 col-lg-8'>
                    <Formik enableReinitialize={true}
                            initialValues={inputs}
                            validationSchema={validationSchema}
                            onSubmit={doSubmit}>
                        {({handleChange, handleSubmit, isSubmitting, handleBlur, touched, values, errors}) => (
                            <Form className='authform' onSubmit={handleSubmit}>
                                <Form.Group controlId="title" className='authform__form-wrapper'>
                                    <Form.Label className={'authform__form-wrapper--label' +
                                    ((touched.title && errors.title) ? 'error' : '')}>
                                        Title
                                    </Form.Label>
                                    <Form.Control type="text" name='title'
                                                  placeholder='Title'
                                                  onChange={handleChange}
                                                  onBlur={(event) => handleFormikBlur(event, handleBlur)}
                                                  value={values.title}
                                                  className={touched.title && errors.title ? "authform__form-wrapper--error" : null}/>
                                    {(touched.title && errors.title) ?
                                        <small
                                            className='authform__form-wrapper--errormessage'>{errors.title}</small> : null}
                                </Form.Group>

                                <Form.Group controlId="description" className='authform__form-wrapper'>
                                    <Form.Label className={'authform__form-wrapper--label' +
                                    ((touched.description && errors.description) ? 'error' : '')}>
                                        Description
                                    </Form.Label>
                                    <Form.Control type="text" name='description'
                                                  placeholder='Description'
                                                  value={values.description}
                                                  onChange={handleChange}
                                                  onBlur={(event) => handleFormikBlur(event, handleBlur)}
                                                  className={touched.description && errors.description ? "authform__form-wrapper--error" : null}/>
                                    {(touched.description && errors.description) ?
                                        <small
                                            className='authform__form-wrapper--errormessage'>{errors.description}</small> : null}
                                </Form.Group>

                                <Form.Group controlId="body" className='authform__form-wrapper'>
                                    <Form.Label className={'authform__form-wrapper--label' +
                                    ((touched.body && errors.body) ? 'error' : '')}>
                                        Body
                                    </Form.Label>
                                    <Form.Control type="text" as="textarea" rows={8}
                                                  name='body'
                                                  value={values.body}
                                                  onChange={handleChange}
                                                  onBlur={(event) => handleFormikBlur(event, handleBlur)}
                                                  className={touched.body && errors.body ? "authform__form-wrapper--error" : null}/>
                                    {(touched.body && errors.body) ?
                                        <small
                                            className='authform__form-wrapper--errormessage'>{errors.body}</small> : null}
                                </Form.Group>

                                <Button variant="primary" type="submit" className='create-article-form__button'
                                        disabled={(isSubmitting || !!errors.body || !!errors.description || !!errors.title)}>
                                    {isSubmitting ?
                                        <Spinner animation="border" variant="secondary" size='sm'/> : 'Submit'}
                                </Button>
                            </Form>
                        )}
                    </Formik>
                </Col>
                <Col className='col-md-6 col-lg-4'>
                    <Form.Group controlId="tag-list" className='tag'>
                        <Form.Label className={'authform__form-wrapper--label tag__lable'}>
                            Tags
                        </Form.Label>
                        <Form.Control type="text" placeholder="New tag"
                                      className='tag__input'
                                      onChange={(event) => doChange(event)}/>
                        {!isEmpty(newTag)
                            ? <input type='button' className='btn btn-info tag__btn'
                                     size='sm' value='Add'
                                     onClick={addToTagList}/>
                            : ''}
                    </Form.Group>
                    <div className='taglist-box'>
                        <ul className='list-unstyled'>
                            {tagList.length > 0 && !loading
                                ? tagList.map((t, i) => {
                                    return (
                                        <li key={i + Math.random()}>
                                            <div className="tag-checkbox">
                                                <input type="checkbox" className="tag-checkbox__input"
                                                       defaultChecked={t.isChecked}
                                                       onClick={() => modifyTag(t)}/>
                                                <label className="form-check-label ml-1" htmlFor="exampleCheck1">
                                                    {t.title}
                                                </label>
                                            </div>
                                        </li>
                                    )

                                })
                                : !loading && tagList.length === 0
                                    ? <span className='taglist-box__place-holder'>
                                        Start to write a New tag!
                                      </span>
                                    : loading
                                        ? <span className='taglist-box__place-holder'>
                                            <Spinner animation="border" variant="info" size='sm'/>
                                          </span>
                                        : false
                            }
                        </ul>
                    </div>
                </Col>
            </Row>
        </>
    );
};

export default NewArticle;