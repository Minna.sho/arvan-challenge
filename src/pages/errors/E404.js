import React from "react";
import {Container} from "react-bootstrap";

const E404 = () => {
    return <>
        <Container className='text-center pt-5'>
            <h1 className='text-danger'>404 Not Found</h1>
            <h4 className='text-secondary'>The resource you looking for is not exists or deleted.</h4>
        </Container>
    </>
};
export default E404;