const initialState = {
    articles: null
};

const articles = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_ALL_ARTICLES':
            if (action.payload === null)
                return {...state, articles: null}
            return {...state, articles: [...action.payload]};
        default:
            return state;
    }
};

export default articles;
