const initialState = {
    token: null,
    loggedIn: false,
    userInfo: {},
};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {...state, ...action.payload, loggedIn: true};
        case 'LOGOUT':
            return {...initialState};
        case 'SET_USER_INFO':
            return {...state, userInfo: {...action.payload}};
        default:
            return state;
    }
};

export default auth;
