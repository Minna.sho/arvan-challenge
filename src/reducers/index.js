import {combineReducers} from "redux";
import auth from "./auth";
import articles from "./articles";

const reducers = combineReducers({
    auth,
    articles
});

export default reducers;
