import {createStore} from "redux";

import reducers from "./reducers";

const getStore = () => {
    try {
        if (localStorage.getItem("store")) {
            const sreailized = JSON.parse(localStorage.getItem("store"));
            return sreailized;
        }
        return undefined;
    } catch (e) {
        return undefined;
    }
};

const setStore = () => {
    try {
        const sreailized = JSON.stringify({
            auth: store.getState().auth,
            articles: store.getState().articles,
        });
        localStorage.setItem("store", sreailized);
    } catch (e) {
    }
};

const store = createStore(
    reducers,
    getStore(),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() => {
    setStore();
});

export default store;
